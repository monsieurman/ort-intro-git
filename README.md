# Introduction git

## Cours 

### Modèle de données

Git est un outil de gestion de version. Il permet de travailler de manière collaborative simplement tout en gardant un historique précis du travail réalisé.

Pour ce faire il se base sur un modèle de données simple qu'il est important de comprendre pour pouvoir utiliser les différentes interfaces, qu'elles soit graphique ou bien qu'il s'agisse de la CLI. Voici les différents objets de ce modèle : 

```ts
// Correspond au contenu d'un fichier
type blob = map<string, string>
// Correspond à un dossier
type tree = map<string, blob | tree>
// Correspond à un "snapshot" du projet
type commit = {
    tree = tree,
    parents = array<parent>
    author = string
    committer = string
    message = string
}
// Type correspondant à n'importe qu'elle objet
type object 
    = commit 
    | tree 
    | commit
// Type alias 
type hash = string
// Tout les objets de git sont adressé par leur hash 
var objects = map<hash, object>
```

Git utilise la fonction de hash SHA-1 pour adresser chaque objets. Que ce soit un commit, un blob etc...Voir : https://en.wikipedia.org/wiki/SHA-1

### CLI

Pour ce cours, je vous fais utiliser la CLI, car c'est celle que vous pourrez utiliser dans la majorité des cas.

Pour un aperçu d'ensemble de la CLI, voir la documentation : https://git-scm.com/docs

Les commandes à connaitres pour commencer :

- git init
- git clone
- git status
- git log
- git diff
- git add
- git checkout
- git commit
- git merge
- git push
- git pull
- git fetch

### Ressources

- Site officiel de git : https://git-scm.com/
- Méthodologie git flow : https://nvie.com/posts/a-successful-git-branching-model/
- Méthodologie trunk based development : https://trunkbaseddevelopment.com/

## TP

Comprendre le processus de création d'une merge/pull request.

Une pull request est un moyen de proposer des changements sur un projet, c'est une des manières principales de travailler en équipe sur un projet.

- [ ] Forker ce projet *(bouton fork en haut à droite de la page d'accueil du projet)*
- [ ] Cloner votre fork du projet sur votre ordinateur (git clone)
- [ ] Creer une branche correspondant à l'animal correspondant à votre nom dans l'issue : https://gitlab.com/MonsieurMan/ort-intro-git/-/issues/1 (git checkout -b \<nom-de-la-branche>)
- [ ] Ajouter le code correspondant au code de l'animal en éditant le fichier animal.js
- [ ] Faire un commit avec ces changements (git add, puis git commit)
- [ ] Pousser ces changements avec votre nouvelle branche sur votre fork (git push)
- [ ] Créer une merge request sur le projet d'origine (via interface web)

## Devoir

- [ ] Mettre à jour votre fork pour contenir ce readme
- [ ] Corriger ceettte faute de frappe
- [ ] Pousser une merge request sur ce projet avec cette correction